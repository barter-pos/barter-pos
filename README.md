# BARTER POS #



### What is BARTER POS? ###

**BARTER POS** (**B**usiness **A**pplications for **R**etail **T**echnology and **E**nterprise **R**esources) is a business application portal
that utilizes **Square** customer point-of-sale APIs.